/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plefebvr <marvin42@free.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 11:21:53 by plefebvr          #+#    #+#             */
/*   Updated: 2015/12/09 17:32:29 by plefebvr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *newl;
	t_list *result;

	result = NULL;
	if (lst)
	{
		newl = (*f)(lst);
		result = newl;
		while (lst)
		{
			newl->next = (*f)(lst->next);
			newl = newl->next;
			lst = lst->next;
		}
	}
	return (result);
}
