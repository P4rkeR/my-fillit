/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plefebvr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 22:58:38 by plefebvr          #+#    #+#             */
/*   Updated: 2015/12/08 14:57:03 by plefebvr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *lst;
	t_list *nextl;

	lst = *alst;
	while (lst)
	{
		nextl = lst->next;
		del(lst->content, lst->content_size);
		free(lst);
		lst = nextl;
	}
	*alst = NULL;
}
